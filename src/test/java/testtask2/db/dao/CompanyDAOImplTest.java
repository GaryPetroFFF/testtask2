package testtask2.db.dao;

import org.junit.Assert;
import org.springframework.dao.EmptyResultDataAccessException;
import testtask2.db.model.CompanyEntity;
import testtask2.db.service.CompanyService;

public class CompanyDAOImplTest {
    private final CompanyService SERVICE = new CompanyService();

    @org.junit.Test
    public void save() {
        CompanyEntity firstCompany = new CompanyEntity();
        firstCompany.setName("Test");
        firstCompany.setAddress("Test");
        firstCompany.setPhoneNumber((long) 1234567890);
        firstCompany.setTin((long) 3456798);

        SERVICE.saveCompany(firstCompany);
        CompanyEntity secondCompany = SERVICE.findCompany(firstCompany.getId());

        Assert.assertEquals(firstCompany.getName(), secondCompany.getName());
        Assert.assertEquals(firstCompany.getAddress(), secondCompany.getAddress());
        Assert.assertEquals(firstCompany.getPhoneNumber(), secondCompany.getPhoneNumber());
        Assert.assertEquals(firstCompany.getTin(), secondCompany.getTin());

        SERVICE.deleteCompany(firstCompany);
    }

    @org.junit.Test
    public void update() {
        final String UPD_NAME = "Update";

        CompanyEntity company = new CompanyEntity();
        company.setName("Test");
        company.setAddress("Test");
        company.setPhoneNumber((long) 1234567890);
        company.setTin((long) 3456798);

        SERVICE.saveCompany(company);
        company.setName(UPD_NAME);
        SERVICE.saveCompany(company);
        company = SERVICE.findCompany(company.getId());

        Assert.assertEquals(UPD_NAME, company.getName());

        SERVICE.deleteCompany(company);
    }

    @org.junit.Test
    public void delete() {
        CompanyEntity company = new CompanyEntity();
        company.setName("Test");
        company.setAddress("Test");
        company.setPhoneNumber((long) 1234567890);
        company.setTin((long) 3456798);

        SERVICE.saveCompany(company);
        long id = company.getId();
        SERVICE.deleteCompany(company);

        Assert.assertThrows(EmptyResultDataAccessException.class, () -> SERVICE.findCompany(id));
    }
}