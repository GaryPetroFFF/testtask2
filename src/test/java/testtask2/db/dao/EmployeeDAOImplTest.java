package testtask2.db.dao;

import org.junit.*;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.springframework.dao.EmptyResultDataAccessException;
import testtask2.db.model.CompanyEntity;
import testtask2.db.model.EmployeeEntity;
import testtask2.db.service.CompanyService;
import testtask2.db.service.EmployeeService;

public class EmployeeDAOImplTest {
    private final EmployeeService SERVICE = new EmployeeService();
    private static long companyId;

    @BeforeClass
    public static void createCompany() {
        CompanyEntity company = new CompanyEntity();
        (new CompanyService()).saveCompany(company);
        companyId = company.getId();
    }

    @AfterClass
    public static void deleteCompany() {
        CompanyService service = new CompanyService();
        CompanyEntity company = service.findCompany(companyId);
        service.deleteCompany(company);
    }

    @Test
    public void save() {
        EmployeeEntity firstEmployee = new EmployeeEntity();
        firstEmployee.setName("Test");
        firstEmployee.setSurname("Test");
        firstEmployee.setEmail("Test");
        firstEmployee.setCompanyId(companyId);

        SERVICE.saveEmployee(firstEmployee);
        EmployeeEntity secondEmployee = SERVICE.findEmployee(firstEmployee.getId());

        Assert.assertEquals(firstEmployee.getName(), secondEmployee.getName());
        Assert.assertEquals(firstEmployee.getSurname(), secondEmployee.getSurname());
        Assert.assertEquals(firstEmployee.getEmail(), secondEmployee.getEmail());

        SERVICE.deleteEmployee(firstEmployee);
    }

    @Test
    public void update() {
        final String UPD_NAME = "Update";

        EmployeeEntity employee = new EmployeeEntity();
        employee.setName("Test");
        employee.setSurname("Test");
        employee.setEmail("Test");
        employee.setCompanyId(companyId);

        SERVICE.saveEmployee(employee);
        employee.setName(UPD_NAME);
        SERVICE.saveEmployee(employee);
        employee = SERVICE.findEmployee(employee.getId());

        Assert.assertEquals(UPD_NAME, employee.getName());

        SERVICE.deleteEmployee(employee);
    }

    @Test
    public void delete() {
        EmployeeEntity employee = new EmployeeEntity();
        employee.setName("Test");
        employee.setSurname("Test");
        employee.setEmail("Test");
        employee.setCompanyId(companyId);

        SERVICE.saveEmployee(employee);
        long id = employee.getId();
        SERVICE.deleteEmployee(employee);

        Assert.assertThrows(EmptyResultDataAccessException.class, () -> SERVICE.findEmployee(id));
    }
}