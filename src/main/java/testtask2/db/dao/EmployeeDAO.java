package testtask2.db.dao;

import testtask2.db.model.EmployeeEntity;

import java.util.List;

public interface EmployeeDAO {
    EmployeeEntity findById(Long id);

    void save(EmployeeEntity employee);

    void update(EmployeeEntity employee);

    void delete(EmployeeEntity employee);

    List<EmployeeEntity> findAll();
}
