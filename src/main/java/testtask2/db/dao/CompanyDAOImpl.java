package testtask2.db.dao;

import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import testtask2.db.model.CompanyEntity;
import testtask2.db.util.CompanyColumnEnum;
import testtask2.db.util.DataSourceSingleton;

import java.util.List;

public class CompanyDAOImpl implements CompanyDAO {
    private final NamedParameterJdbcTemplate TEMPLATE;
    private final RowMapper<CompanyEntity> COMPANY_ROW_MAPPER = new BeanPropertyRowMapper<CompanyEntity>(CompanyEntity.class);

    public CompanyDAOImpl() throws IllegalStateException {
        TEMPLATE = new NamedParameterJdbcTemplate(DataSourceSingleton.getInstance());
    }

    public CompanyEntity findById(Long id) {
        SqlParameterSource params = new MapSqlParameterSource()
                .addValue(CompanyColumnEnum.ID.getString(), id);
        CompanyEntity company = TEMPLATE.queryForObject(Sql.FIND_BY_ID_SQL, params, COMPANY_ROW_MAPPER);
        company.setId(id);
        return company;
    }

    public void save(CompanyEntity company) {
        KeyHolder holder = new GeneratedKeyHolder();
        SqlParameterSource params = new MapSqlParameterSource()
                .addValue(CompanyColumnEnum.NAME.getString(), company.getName())
                .addValue(CompanyColumnEnum.TIN.getString(), company.getTin())
                .addValue(CompanyColumnEnum.ADDRESS.getString(), company.getAddress())
                .addValue(CompanyColumnEnum.PHONE_NUMBER.getString(), company.getPhoneNumber());
        TEMPLATE.update(Sql.SAVE_SQL, params, holder, new String[] { CompanyColumnEnum.ID.getString() });
        company.setId(holder.getKey().longValue());
    }

    public void update(CompanyEntity company) {
        SqlParameterSource params = new MapSqlParameterSource()
                .addValue(CompanyColumnEnum.NAME.getString(), company.getName())
                .addValue(CompanyColumnEnum.TIN.getString(), company.getTin())
                .addValue(CompanyColumnEnum.ADDRESS.getString(), company.getAddress())
                .addValue(CompanyColumnEnum.PHONE_NUMBER.getString(), company.getPhoneNumber())
                .addValue(CompanyColumnEnum.ID.getString(), company.getId());
        TEMPLATE.update(Sql.UPDATE_SQL, params);
    }

    public void delete(CompanyEntity company) {
        SqlParameterSource params = new MapSqlParameterSource()
                .addValue(CompanyColumnEnum.ID.getString(), company.getId());
        TEMPLATE.update(Sql.DELETE_SQL, params);
    }

    public List<CompanyEntity> findAll() {
        return TEMPLATE.query(Sql.FIND_ALL_SQL, COMPANY_ROW_MAPPER);
    }

    static class Sql {
        static final String FIND_BY_ID_SQL = "SELECT * FROM companies WHERE " + CompanyColumnEnum.ID.getString() +
                " = :" + CompanyColumnEnum.ID.getString();
        static final String SAVE_SQL = "INSERT INTO companies (" + CompanyColumnEnum.NAME.getString() + ", "
                + CompanyColumnEnum.TIN.getString() + ", " + CompanyColumnEnum.ADDRESS.getString() + ", "
                + CompanyColumnEnum.PHONE_NUMBER.getString() + ") VALUES (:" + CompanyColumnEnum.NAME.getString() + ", :"
                + CompanyColumnEnum.TIN.getString() + ", :" + CompanyColumnEnum.ADDRESS.getString() + ", :"
                + CompanyColumnEnum.PHONE_NUMBER.getString() + ")";
        static final String UPDATE_SQL = "UPDATE companies SET " + CompanyColumnEnum.NAME.getString() + " = :"
                + CompanyColumnEnum.NAME.getString() + ", " + CompanyColumnEnum.TIN.getString() + " = :"
                + CompanyColumnEnum.TIN.getString() + ", " + CompanyColumnEnum.ADDRESS.getString() + " = :"
                + CompanyColumnEnum.ADDRESS.getString() + ", " + CompanyColumnEnum.PHONE_NUMBER.getString() + " = :"
                + CompanyColumnEnum.PHONE_NUMBER.getString() + " WHERE " + CompanyColumnEnum.ID.getString() + " = :"
                + CompanyColumnEnum.ID.getString();
        static final String DELETE_SQL = "DELETE FROM companies WHERE " + CompanyColumnEnum.ID.getString() + " = :"
                + CompanyColumnEnum.ID.getString();
        static final String FIND_ALL_SQL = "SELECT * FROM companies";
    }
}
