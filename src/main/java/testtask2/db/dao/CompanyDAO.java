package testtask2.db.dao;

import testtask2.db.model.CompanyEntity;

import java.util.List;

public interface CompanyDAO {

    CompanyEntity findById(Long id);

    void save(CompanyEntity company);

    void update(CompanyEntity company);

    void delete(CompanyEntity company);

    List<CompanyEntity> findAll();
}
