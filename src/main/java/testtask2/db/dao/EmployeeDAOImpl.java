package testtask2.db.dao;

import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import testtask2.db.model.CompanyEntity;
import testtask2.db.model.EmployeeEntity;
import testtask2.db.service.CompanyService;
import testtask2.db.util.DataSourceSingleton;
import testtask2.db.util.EmployeeColumnEnum;

import java.util.List;

public class EmployeeDAOImpl implements EmployeeDAO {
    private final NamedParameterJdbcTemplate TEMPLATE;
    private final RowMapper<EmployeeEntity> EMPLOYEE_ROW_MAPPER = new BeanPropertyRowMapper<EmployeeEntity>(EmployeeEntity.class);

    public EmployeeDAOImpl() throws IllegalStateException {
        TEMPLATE = new NamedParameterJdbcTemplate(DataSourceSingleton.getInstance());
    }

    public EmployeeEntity findById(Long id) {
        SqlParameterSource params = new MapSqlParameterSource()
                .addValue(EmployeeColumnEnum.ID.getString(), id);
        EmployeeEntity employee = TEMPLATE.queryForObject(Sql.FIND_BY_ID_SQL, params, EMPLOYEE_ROW_MAPPER);
        employee.setId(id);
        return employee;
    }

    public void save(EmployeeEntity employee) {
        KeyHolder holder = new GeneratedKeyHolder();
        SqlParameterSource params = new MapSqlParameterSource()
                .addValue(EmployeeColumnEnum.NAME.getString(), employee.getName())
                .addValue(EmployeeColumnEnum.SURNAME.getString(), employee.getSurname())
                .addValue(EmployeeColumnEnum.BIRTH_DATE.getString(), employee.getBirthDate())
                .addValue(EmployeeColumnEnum.EMAIL.getString(), employee.getEmail())
                .addValue(EmployeeColumnEnum.COMPANY_ID.getString(), employee.getCompanyId());
        TEMPLATE.update(Sql.SAVE_SQL, params, holder, new String[] { EmployeeColumnEnum.ID.getString() });
        employee.setId(holder.getKey().longValue());
    }

    public void update(EmployeeEntity employee) {
        SqlParameterSource params = new MapSqlParameterSource()
                .addValue(EmployeeColumnEnum.NAME.getString(), employee.getName())
                .addValue(EmployeeColumnEnum.SURNAME.getString(), employee.getSurname())
                .addValue(EmployeeColumnEnum.BIRTH_DATE.getString(), employee.getBirthDate())
                .addValue(EmployeeColumnEnum.EMAIL.getString(), employee.getEmail())
                .addValue(EmployeeColumnEnum.COMPANY_ID.getString(), employee.getCompanyId())
                .addValue(EmployeeColumnEnum.ID.getString(), employee.getId());
        TEMPLATE.update(Sql.UPDATE_SQL, params);
    }

    public void delete(EmployeeEntity employee) {
        SqlParameterSource params = new MapSqlParameterSource()
                .addValue(EmployeeColumnEnum.ID.getString(), employee.getId());
        TEMPLATE.update(Sql.DELETE_SQL, params);
    }

    public List<EmployeeEntity> findAll() {
        List<EmployeeEntity> employeeList = TEMPLATE.query(Sql.FIND_ALL_SQL, EMPLOYEE_ROW_MAPPER);
        for (EmployeeEntity employee : employeeList) {
            try {
                CompanyEntity company = (new CompanyService()).findCompany(employee.getCompanyId());
                employee.setCompanyName(company.getName());
            } catch (EmptyResultDataAccessException ex) {
                employee.setCompanyId(null);
            }
        }
        return employeeList;
    }

    static class Sql {
        static final String FIND_BY_ID_SQL = "SELECT * FROM employees WHERE " + EmployeeColumnEnum.ID.getString() +
                " = :" + EmployeeColumnEnum.ID.getString();
        static final String SAVE_SQL = "INSERT INTO employees (" + EmployeeColumnEnum.NAME.getString() + ", "
                + EmployeeColumnEnum.SURNAME.getString() + "," + EmployeeColumnEnum.BIRTH_DATE.getString() + ", "
                + EmployeeColumnEnum.EMAIL.getString() + ", " + EmployeeColumnEnum.COMPANY_ID.getString()
                + ") VALUES (:" + EmployeeColumnEnum.NAME.getString() + ", :" + EmployeeColumnEnum.SURNAME.getString()
                + ", :" + EmployeeColumnEnum.BIRTH_DATE.getString() + ", :" + EmployeeColumnEnum.EMAIL.getString()
                + ", :" + EmployeeColumnEnum.COMPANY_ID.getString() + ")";
        static final String UPDATE_SQL = "UPDATE employees SET " + EmployeeColumnEnum.NAME.getString() + " = :"
                + EmployeeColumnEnum.NAME.getString() + ", " + EmployeeColumnEnum.SURNAME.getString() + " = :"
                + EmployeeColumnEnum.SURNAME.getString() + ", " + EmployeeColumnEnum.BIRTH_DATE.getString() + " = :"
                + EmployeeColumnEnum.BIRTH_DATE.getString() + ", " + EmployeeColumnEnum.EMAIL.getString() + " = :"
                + EmployeeColumnEnum.EMAIL.getString() + ", " + EmployeeColumnEnum.COMPANY_ID.getString() + " = :"
                + EmployeeColumnEnum.COMPANY_ID.getString() + " WHERE " + EmployeeColumnEnum.ID.getString() + " = :"
                + EmployeeColumnEnum.ID.getString();
        static final String DELETE_SQL = "DELETE FROM employees WHERE " + EmployeeColumnEnum.ID.getString() + " = :"
                + EmployeeColumnEnum.ID.getString();
        static final String FIND_ALL_SQL = "SELECT * FROM employees";
    }
}
