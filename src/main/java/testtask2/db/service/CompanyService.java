package testtask2.db.service;

import testtask2.db.dao.CompanyDAO;
import testtask2.db.dao.CompanyDAOImpl;
import testtask2.db.model.CompanyEntity;
import testtask2.db.model.EmployeeEntity;

import java.util.List;

public class CompanyService {
    private final CompanyDAO dao;

    public CompanyService() throws IllegalStateException{
        dao = new CompanyDAOImpl();
    }

    public CompanyEntity findCompany(Long id) { return dao.findById(id); }

    public void saveCompany(CompanyEntity company) { dao.save(company); }

    public void updateCompany(CompanyEntity company) { dao.update(company); }

    public void deleteCompany(CompanyEntity company) {
        dao.delete(company); }

    public List<CompanyEntity> findAllCompanies() { return dao.findAll(); }
}
