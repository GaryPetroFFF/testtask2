package testtask2.db.service;

import org.springframework.dao.EmptyResultDataAccessException;
import testtask2.db.dao.CompanyDAO;
import testtask2.db.dao.CompanyDAOImpl;
import testtask2.db.dao.EmployeeDAO;
import testtask2.db.dao.EmployeeDAOImpl;
import testtask2.db.model.CompanyEntity;
import testtask2.db.model.EmployeeEntity;

import java.util.List;

public class EmployeeService {
    private final EmployeeDAO employeeDAO;
    private final CompanyDAO companyDAO;

    public EmployeeService() throws IllegalStateException {
        employeeDAO = new EmployeeDAOImpl();
        companyDAO = new CompanyDAOImpl();
    }

    public EmployeeEntity findEmployee(Long id) {
        EmployeeEntity employee = employeeDAO.findById(id);
        try {
            CompanyEntity company = companyDAO.findById(employee.getCompanyId());
            employee.setCompanyName(company.getName());
        } catch (EmptyResultDataAccessException ex) {
            employee.setCompanyId(null);
        }
        return employee;
    }

    public void saveEmployee(EmployeeEntity employee) throws EmptyResultDataAccessException {
        companyDAO.findById(employee.getCompanyId());
        employeeDAO.save(employee);
    }

    public void forceSaveEmployee(EmployeeEntity employee) {
        employeeDAO.save(employee);
    }

    public void updateEmployee(EmployeeEntity employee) throws EmptyResultDataAccessException {
        companyDAO.findById(employee.getCompanyId());
        employeeDAO.update(employee);
    }

    public void forceUpdateEmployee(EmployeeEntity employee) { employeeDAO.update(employee); }

    public void deleteEmployee(EmployeeEntity employee) { employeeDAO.delete(employee); }

    public List<EmployeeEntity> findAllEmployees() { return employeeDAO.findAll(); }

}
