package testtask2.db.util;

public enum CompanyColumnEnum {
    ID("id"),
    NAME("name"),
    TIN("tin"),
    ADDRESS("address"),
    PHONE_NUMBER("phone_number");

    private final String string;

    CompanyColumnEnum(String string) { this.string = string; }

    public String getString() { return string; }
}
