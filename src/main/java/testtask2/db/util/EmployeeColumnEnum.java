package testtask2.db.util;

public enum EmployeeColumnEnum {
    ID("id"),
    NAME("name"),
    SURNAME("surname"),
    BIRTH_DATE("birth_date"),
    EMAIL("email"),
    COMPANY_ID("company_id");

    private final String string;

    EmployeeColumnEnum(String string) { this.string = string; }

    public String getString() { return string; }
}
