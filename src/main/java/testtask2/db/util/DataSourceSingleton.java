package testtask2.db.util;

import org.springframework.jdbc.datasource.DriverManagerDataSource;

import javax.sql.DataSource;

public class DataSourceSingleton {
    private static DriverManagerDataSource dataSource = null;

    public static DataSource getInstance() throws IllegalStateException {
        if (dataSource == null) {
            dataSource = new DriverManagerDataSource();
            dataSource.setDriverClassName("org.postgresql.Driver");
            dataSource.setUrl("jdbc:postgresql://localhost:5432/postgres");
            dataSource.setUsername("postgres");
            dataSource.setPassword("3456798");
        }
        return dataSource;
    }
}
