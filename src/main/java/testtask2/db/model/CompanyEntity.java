package testtask2.db.model;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "companies")
public class CompanyEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "name")
    private String name;
    @Column(name = "tin")
    private long tin;
    @Column(name = "address")
    private String address;
    @Column(name = "phone_number")
    private long phoneNumber;

    public Long getId() { return id; }

    public String getName() { return name; }

    public Long getTin() { return tin; }

    public String getAddress() { return address; }

    public Long getPhoneNumber() { return phoneNumber; }

    public void setId(Long id) { this.id = id; }

    public void setName(String name) { this.name = name; }

    public void setTin(Long tin) { this.tin = tin; }

    public void setAddress(String address) { this.address = address; }

    public void setPhoneNumber(Long phoneNumber) { this.phoneNumber = phoneNumber; }
}
