package testtask2.view;

import com.vaadin.annotations.Title;
import com.vaadin.server.VaadinRequest;
import com.vaadin.ui.*;
import testtask2.view.tab.CompanyGridTabImpl;
import testtask2.view.tab.EmployeeGridTabImpl;
import testtask2.view.tab.GridTabAbst;
import java.util.logging.Level;
import java.util.logging.Logger;

@Title("Test Task Vaadin")
public class StartPageUI extends UI {

    @Override
    public void init(VaadinRequest request) {
        VerticalLayout baseUI = new VerticalLayout();
        TabSheet tabSheet = new TabSheet();

        HorizontalLayout buttons = new HorizontalLayout();
        try {
            tabSheet.addComponents((new CompanyGridTabImpl()).create(this), (new EmployeeGridTabImpl()).create(this));
        } catch (IllegalStateException ex) {
            Notification.show("Ошибка конфигурации соединения с базой данных", Notification.Type.ERROR_MESSAGE);
            Logger.getLogger((StartPageUI.class.getName())).log(Level.SEVERE, null, ex);
        }

        buttons.addComponent(new Button("Добавить",
                click -> {
                    ((GridTabAbst) tabSheet.getSelectedTab()).createAddModalWindow();
                }));
        buttons.addComponent(new Button("Редактировать",
                click -> {
                    for (Object object : ((GridTabAbst) tabSheet.getSelectedTab()).getSelectedItems()) {
                        ((GridTabAbst) tabSheet.getSelectedTab()).createEditModalWindow(object);
                    }
                }));
        buttons.addComponent(new Button("Удалить",
                click -> {
                    ((GridTabAbst) tabSheet.getSelectedTab()).deleteSelected();
                }));

        baseUI.addComponent(buttons);
        baseUI.addComponent(tabSheet);
        setContent(baseUI);
    }

}
