package testtask2.view.tab;

import com.vaadin.ui.Grid;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;

import java.util.Set;

public abstract class GridTabAbst<T> extends VerticalLayout {
    protected Grid grid;

    public abstract VerticalLayout create(UI parent);

    public abstract Set<T> getSelectedItems();

    public abstract void createAddModalWindow();

    public abstract void createEditModalWindow(Object object);

    public abstract void deleteSelected();

}
