package testtask2.view.tab;

import com.vaadin.ui.*;
import org.springframework.dao.EmptyResultDataAccessException;
import testtask2.db.model.EmployeeEntity;
import testtask2.db.service.EmployeeService;

import java.sql.Date;
import java.time.ZoneId;
import java.util.Set;

public class EmployeeGridTabImpl extends GridTabAbst {
    private UI parent;
    private EmployeeService service;

    @Override
    public VerticalLayout create(UI parent) throws IllegalStateException {
        service = new EmployeeService();
        this.parent = parent;
        this.setCaption("Сотрудники");
        grid = new Grid<>(EmployeeEntity.class);
        grid.removeColumn("id");
        grid.removeColumn("companyId");
        grid.setColumnOrder("name", "surname", "birthDate", "email", "companyName");
        grid.setItems(service.findAllEmployees());
        this.addComponent(grid);

        return this;
    }
    @Override
    public Set<EmployeeEntity> getSelectedItems() { return grid.getSelectedItems(); }

    @Override
    public void createAddModalWindow() {
        Window modalWindow = new Window("Добавить сотрудника");
        VerticalLayout verticalLayout = new VerticalLayout();
        TextField nameTextField = new TextField("Имя");
        TextField surnameTextField = new TextField("Фамилия");
        DateField birthDateTextField = new DateField("Дата рождения");
        TextField emailTextField = new TextField("Электронная почта");
        TextField companyIdTextField = new TextField("ID компании");
        verticalLayout.addComponent(nameTextField);
        verticalLayout.addComponent(surnameTextField);
        verticalLayout.addComponent(birthDateTextField);
        verticalLayout.addComponent(emailTextField);
        verticalLayout.addComponent(companyIdTextField);

        HorizontalLayout horizontalLayout = new HorizontalLayout();
        horizontalLayout.addComponent(new Button("Отмена",
                click -> modalWindow.close()));
        horizontalLayout.addComponent(new Button("Сохранить",
                click -> {
                    EmployeeEntity employee = new EmployeeEntity();
                    employee.setName(nameTextField.getValue());
                    employee.setSurname(surnameTextField.getValue());
                    if (birthDateTextField.getValue() == null) {
                        employee.setBirthDate(null);
                    } else {
                        employee.setBirthDate(Date.valueOf(birthDateTextField.getValue()));
                    }
                    employee.setEmail(emailTextField.getValue());
                    employee.setCompanyId(Long.parseLong(companyIdTextField.getValue()));
                    try {
                        service.saveEmployee(employee);
                        grid.setItems(service.findAllEmployees());
                        modalWindow.close();
                    } catch (EmptyResultDataAccessException ex) {
                        Window emptyResultDataAccessWindow = new Window();
                        VerticalLayout windowBase = new VerticalLayout();
                        windowBase.addComponent(new Label(
                                "Не существует компании с заданным ID"
                        ));
                        HorizontalLayout buttons = new HorizontalLayout();
                        buttons.addComponent(new Button("Отмена сохранения",
                                cancelClick -> {
                                    emptyResultDataAccessWindow.close();
                                }));
                        buttons.addComponent(new Button("Сохранить принудительно",
                                forceSaveClick -> {
                                    service.forceSaveEmployee(employee);
                                    emptyResultDataAccessWindow.close();
                                    grid.setItems(service.findAllEmployees());
                                    modalWindow.close();
                                }));
                        windowBase.addComponent(buttons);
                        emptyResultDataAccessWindow.setContent(windowBase);
                        emptyResultDataAccessWindow.center();
                        parent.addWindow(emptyResultDataAccessWindow);
                    }
                }));
        verticalLayout.addComponent(horizontalLayout);
        modalWindow.setContent(verticalLayout);
        modalWindow.center();

        parent.addWindow(modalWindow);
    }

    @Override
    public void createEditModalWindow(Object object) {
        EmployeeEntity employee = (EmployeeEntity) object;
        Window modalWindow = new Window("Добавить сотрудника");
        VerticalLayout verticalLayout = new VerticalLayout();
        TextField nameTextField = new TextField("Имя");
        nameTextField.setValue(employee.getName());
        TextField surnameTextField = new TextField("Фамилия");
        surnameTextField.setValue(employee.getSurname());
        DateField birthDateTextField = new DateField("Дата рождения");
        TextField companyIdTextField = new TextField("ID компании");
        companyIdTextField.setValue(String.valueOf(employee.getCompanyId()));
        if (employee.getBirthDate() == null) {
            birthDateTextField.setValue(null);
        } else {
            birthDateTextField.setValue(employee.getBirthDate().toInstant().atZone(ZoneId.systemDefault()).toLocalDate());
        }
        TextField emailTextField = new TextField("Электронная почта");
        emailTextField.setValue(employee.getEmail());
        verticalLayout.addComponent(nameTextField);
        verticalLayout.addComponent(surnameTextField);
        verticalLayout.addComponent(birthDateTextField);
        verticalLayout.addComponent(emailTextField);
        verticalLayout.addComponent(companyIdTextField);

        HorizontalLayout horizontalLayout = new HorizontalLayout();
        horizontalLayout.addComponent(new Button("Отмена",
                click -> modalWindow.close()));
        horizontalLayout.addComponent(new Button("Сохранить",
                click -> {
                    employee.setName(nameTextField.getValue());
                    employee.setSurname(surnameTextField.getValue());
                    if (birthDateTextField.getValue() == null) {
                        employee.setBirthDate(null);
                    } else {
                        employee.setBirthDate(Date.valueOf(birthDateTextField.getValue()));
                    }
                    employee.setEmail(emailTextField.getValue());
                    employee.setCompanyId(Long.parseLong(companyIdTextField.getValue()));
                    try {
                        service.updateEmployee(employee);
                        grid.setItems(service.findAllEmployees());
                        modalWindow.close();
                    } catch (EmptyResultDataAccessException ex) {
                        Window emptyResultDataAccessWindow = new Window();
                        VerticalLayout windowBase = new VerticalLayout();
                        windowBase.addComponent(new Label(
                                "Не существует компании с заданным ID"
                        ));
                        HorizontalLayout buttons = new HorizontalLayout();
                        buttons.addComponent(new Button("Отмена редактирования",
                                cancelClick -> {
                                    emptyResultDataAccessWindow.close();
                                }));
                        buttons.addComponent(new Button("Редактировать принудительно",
                                forceSaveClick -> {
                                    service.forceUpdateEmployee(employee);
                                    emptyResultDataAccessWindow.close();
                                    grid.setItems(service.findAllEmployees());
                                    modalWindow.close();
                                }));
                        windowBase.addComponent(buttons);
                        emptyResultDataAccessWindow.setContent(windowBase);
                        emptyResultDataAccessWindow.center();
                        parent.addWindow(emptyResultDataAccessWindow);
                    }
                }));
        verticalLayout.addComponent(horizontalLayout);
        modalWindow.setContent(verticalLayout);
        modalWindow.center();

        parent.addWindow(modalWindow);
    }

    @Override
    public void deleteSelected() {
        Set<EmployeeEntity> deleteSet = grid.getSelectedItems();
        for (EmployeeEntity employee : deleteSet) {
            service.deleteEmployee(employee);
        }
        grid.setItems(service.findAllEmployees());
    }
}
