package testtask2.view.tab;

import com.vaadin.ui.*;
import testtask2.db.model.CompanyEntity;
import testtask2.db.service.CompanyService;

import java.util.Set;

public class CompanyGridTabImpl extends GridTabAbst {
    private UI parent;
    private CompanyService service;

    @Override
    public VerticalLayout create(UI parent) throws IllegalStateException {
        service = new CompanyService();
        this.parent = parent;
        this.setCaption("Компании");
        grid = new Grid<>(CompanyEntity.class);
        grid.removeColumn("id");
        grid.setColumnOrder("name", "tin", "address", "phoneNumber");
        grid.setItems(service.findAllCompanies());
        this.addComponent(grid);

        return this;
    }
    @Override
    public Set<CompanyEntity> getSelectedItems() {
        return grid.getSelectedItems();
    }

    @Override
    public void createAddModalWindow() {
        Window modalWindow = new Window("Добавить компанию");
        VerticalLayout verticalLayout = new VerticalLayout();
        TextField nameTextField = new TextField("Название");
        TextField tinTextField = new TextField("ИНН");
        TextField addressTextField = new TextField("Адрес");
        TextField phoneNumberTextField = new TextField("Телефонный номер");
        verticalLayout.addComponent(nameTextField);
        verticalLayout.addComponent(tinTextField);
        verticalLayout.addComponent(addressTextField);
        verticalLayout.addComponent(phoneNumberTextField);

        HorizontalLayout horizontalLayout = new HorizontalLayout();
        horizontalLayout.addComponent(new Button("Отмена",
                click -> modalWindow.close()));
        horizontalLayout.addComponent(new Button("Сохранить",
                click -> {
                    CompanyEntity company = new CompanyEntity();
                    company.setName(nameTextField.getValue());
                    company.setTin(Long.parseLong(tinTextField.getValue()));
                    company.setAddress(addressTextField.getValue());
                    company.setPhoneNumber(Long.parseLong(phoneNumberTextField.getValue()));
                    service.saveCompany(company);
                    grid.setItems(service.findAllCompanies());
                    modalWindow.close();
                }));
        verticalLayout.addComponent(horizontalLayout);
        modalWindow.setContent(verticalLayout);
        modalWindow.center();
        parent.addWindow(modalWindow);
    }

    @Override
    public void createEditModalWindow(Object object) {
        CompanyEntity company = (CompanyEntity) object;
        Window modalWindow = new Window("Добавить компанию");
        VerticalLayout verticalLayout = new VerticalLayout();
        TextField nameTextField = new TextField("Название");
        nameTextField.setValue(company.getName());
        TextField tinTextField = new TextField("ИНН");
        tinTextField.setValue(company.getTin().toString());
        TextField addressTextField = new TextField("Адрес");
        addressTextField.setValue(company.getAddress());
        TextField phoneNumberTextField = new TextField("Телефонный номер");
        phoneNumberTextField.setValue(company.getPhoneNumber().toString());
        verticalLayout.addComponent(nameTextField);
        verticalLayout.addComponent(tinTextField);
        verticalLayout.addComponent(addressTextField);
        verticalLayout.addComponent(phoneNumberTextField);

        HorizontalLayout horizontalLayout = new HorizontalLayout();
        horizontalLayout.addComponent(new Button("Отмена",
                click -> modalWindow.close()));
        horizontalLayout.addComponent(new Button("Сохранить",
                click -> {
                    company.setName(nameTextField.getValue());
                    company.setTin(Long.parseLong(tinTextField.getValue()));
                    company.setAddress(addressTextField.getValue());
                    company.setPhoneNumber(Long.parseLong(phoneNumberTextField.getValue()));
                    service.updateCompany(company);
                    grid.setItems(service.findAllCompanies());
                    modalWindow.close();
                }));
        verticalLayout.addComponent(horizontalLayout);
        modalWindow.setContent(verticalLayout);
        modalWindow.center();

        parent.addWindow(modalWindow);
    }

    @Override
    public void deleteSelected() {
        Set<CompanyEntity> deleteSet = grid.getSelectedItems();
        for (CompanyEntity company : deleteSet) {
            service.deleteCompany(company);
        }
        grid.setItems(service.findAllCompanies());
    }
}
